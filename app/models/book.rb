class Book < ApplicationRecord
  belongs_to :author
  belongs_to :category
  has_many :reservations, dependent: :destroy
end
