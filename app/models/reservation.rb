class Reservation < ApplicationRecord
  belongs_to :client
  belongs_to :book
  belongs_to :librarian

  after_create :decrease
  after_destroy :increase

  def decrease
    self.book.stock = self.book.stock - 1
    self.book.save
  end

  def increase
    self.book.stock = self.book.stock + 1
    self.book.save
  end
  
end
