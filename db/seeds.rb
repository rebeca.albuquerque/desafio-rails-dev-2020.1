# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

print "Gerando nomes de Autores(Authors) ... "
    Author.create!([{name:"Monteiro Lobato"},
                {name:"José de Alencar"},
                {name:"Carlos Drummond de Andrade"},
                {name:"Machado de Assis"},
                {name:"João Cabral de Melo Neto"}])
puts "[OK]"

print "Gerando Clientes(Clients) ... "
    Client.create!([{name:"Larissa Manoela"},
                {name:"Moranguinho"},
                {name:"Mundinho JK Rowling Br"},
                {name:"Babu"},
                {name:"Corona"}])
puts "[OK]"

print "Gerando Categorias(Categories) ... "
    Category.create!([{name:"Comedia"},
                {name:"Romance"},
                {name:"Ação"},
                {name:"Aventura"},
                {name:"Poesia"}])
puts "[OK]"

print "Gerando nomes de Livros(Books) ... "
    Book.create!([{name:"Auto do Frade", 
                stock: 5, 
                author: Author.all.sample, 
                category: Category.all.sample},
                {name:"O sertão", 
                stock: 7, 
                author: Author.all.sample, 
                category: Category.all.sample},
                {name:"Dom Casmurro", 
                stock: 15, 
                author: Author.all.sample, 
                category: Category.all.sample}])
puts "[OK]"

print "Gerando nomes dos Usuarios(Users) ... "
    Librarian.create!([{name:"Selena Gomez",
                email:"selenagomez@gmail.com",
                password:"123456",
                password_confirmation:"123456"}, 
                {name:"Anitta",
                email:"anitta@gmail.com",
                password:"123456",
                password_confirmation:"123456"}])
puts "[OK]"

print "Gerando reservas(Reservations) ... "    
    Reservation.create!([{book:Book.all.sample,
                client: Client.all.sample,
                librarian: Librarian.all.sample}])
puts "[OK]"


