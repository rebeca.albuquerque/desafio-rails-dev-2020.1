Rails.application.routes.draw do
  get 'menu/index'
  root to:  'menu#index'
  resources :reservations
  resources :books
  resources :categories
  resources :authors
  resources :clients
  devise_for :librarians
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
